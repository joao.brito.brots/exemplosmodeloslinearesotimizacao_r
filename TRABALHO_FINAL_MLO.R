####
## TRABALHO FINAL
# 
# Pacotes de trabalho
library(tidyverse)
#install.packages("lpSolveAPI")
library(lpSolveAPI)

# Cria modelo
modelo <- make.lp(0, 4)

# Indica se e "min" minimizacao ou "max" maximizacao
lp.control(
  lprec = modelo, 
  sense = "max"
)

set.type(modelo, 1:4, "integer")

set.objfn(
  lprec = modelo,
  obj = c(400, 564.38,534.19,960)
)

# Restricoes
add.constraint(modelo, c(1,1,1,1) , "<=", 4790)
add.constraint(modelo, c(0,0,0,0.4) , "<=", 264)
add.constraint(modelo, c(0,0,0,0.23) , "<=", 416)
add.constraint(modelo, c(0,0.02,0.02,0) , "<=", 40)
add.constraint(modelo, c(1,0,0,1) , "<=", 1850)
add.constraint(modelo, c(0,0,1,0) , "<=", 150)
add.constraint(modelo, c(0,1,0,0) , "<=", 4640)

solve(modelo)
get.objective(modelo)
get.variables(modelo)
get.sensitivity.rhs(modelo)
get.sensitivity.obj(modelo)