# Pacote com solver
library(lpSolveAPI)

# Exercicio 1 da lista 05

# Cria modelo
modelo <- make.lp(
  nrow = 4, 
  ncol = 4
)

# Indica se e "min" minimizacao ou "max" maximizacao
lp.control(
  lprec = modelo, 
  sense = "max"
)

# Funcao objetivo
set.objfn(
  lprec = modelo,
  obj = c(0.1, 0.085, 0.095, 0.125)
)

# Restricoes
add.constraint(modelo, c(1, 1, 1, 1) , "<=", 650000)
add.constraint(modelo, c(0, 0, 0, 1) , "<=", 162500)
add.constraint(modelo, c(0, 1, 0, -1), ">=",      0)    
add.constraint(modelo, c(1, 0, 0, -1), ">=",      0) 

# Resolucao do problema
solve(modelo)

# Exibe resultado funcao objetivo
get.objective(modelo)
# Exibe resultado das variaveis
get.variables(modelo)
